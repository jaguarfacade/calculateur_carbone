export const removeAccents = (str) => {
    const accentMap = {
      'á': 'a', 'â': 'a', 'à': 'a', 'ã': 'a', 'ä': 'a',
      'é': 'e', 'ê': 'e', 'è': 'e', 'ë': 'e',
      'í': 'i', 'î': 'i', 'ì': 'i', 'ï': 'i',
      'ó': 'o', 'ô': 'o', 'ò': 'o', 'õ': 'o', 'ö': 'o',
      'ú': 'u', 'û': 'u', 'ù': 'u', 'ü': 'u',
      'ç': 'c'
    };
    return str.split('').map((char) => accentMap[char] || char).join('');
  };


export function accentInsensitiveRegex(str) {
  const accentMap = {
    'a': '[aàáâãäå]',
    'e': '[eéèêë]',
    'i': '[iíìîï]',
    'o': '[oóòôõö]',
    'u': '[uúùûü]',
    // ajoute d'autres caractères si nécessaire
  };
  
  let regexStr = str.toLowerCase();
    
  for (let char in accentMap) {
    regexStr = regexStr.replace(new RegExp(char, 'g'), accentMap[char]);
  }
    
  return new RegExp(regexStr);
}