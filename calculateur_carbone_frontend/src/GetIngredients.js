import axios from 'axios';

export const getIngredients = async () => {
  try {
    const response = await axios.get('http://localhost:5000/ingredients');
    // console.log("Chargement de la base de données OK");
    // console.log(response.data)
    return response.data;
  } catch (error) {
    console.error('Erreur lors de la récupération des données:', error);
    return null;
  }
};