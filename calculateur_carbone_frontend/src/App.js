import logo from './logo.svg';
import React, { useState, useEffect } from 'react';
import { getIngredients } from './GetIngredients';
import './App.css';
import TextField from './TextField';
import IngredientsArea from './IngredientsArea';
import { removeAccents , accentInsensitiveRegex } from './Utils'


function App() {
  // Récupération base de données
  const [ingredients, setIngredients] = useState([]);
  useEffect(() => {
    getIngredients().then(data => {
      setIngredients(data);
    });
  }, []);
  //console.log(ingredients[0])

  // Définition
  const [recipeName, setRecipeName] = useState('');
  const [numberOfPortions, setNumberOfPortions] = useState('');
  const [searchQuery, setSearchQuery] = useState([]);
  const [selectedIngredients, setSelectedIngredients] = useState([]);
  const [searchBars, setSearchBars] = useState([]);
  const [filteredIngredients, setFilteredIngredients] = useState([]);
  const [selectedIngredientQuantities, setSelectedIngredientQuantities] = useState([]);


  const handleSearchQuery = (query, index) => {
    const newQuery = [...searchQuery];
    if (typeof newQuery[index] === 'undefined') {
      newQuery[index] = '';
    }
    newQuery[index] = (query);
    setSearchQuery(newQuery);
    console.log("Query : "+ newQuery[index])
  
    const regexQuery = accentInsensitiveRegex(newQuery[index]);
    console.log("RegexQuery : "+ regexQuery)
    const newFilteredIngredients = [...filteredIngredients];
    const filtered = ingredients.filter(ingredient => 
      regexQuery.test(ingredient['Nom du Produit en Français'].toLowerCase())
    );
    newFilteredIngredients[index] = filtered;
    setFilteredIngredients(newFilteredIngredients);
  };
  
  

  const handleSelection = (ingredientName, index) => {
    const ingredientObject = ingredients.find(ing => (ing['Nom du Produit en Français']) === (ingredientName[index]));
    const newSelectedIngredients = [...selectedIngredients];
    newSelectedIngredients[index] = ingredientObject;
    setSelectedIngredients(newSelectedIngredients);
    console.log(newSelectedIngredients)
  };

  const addSearchBar = () => {
    setSearchBars([...searchBars, {}]);
  };

  const handleQuantityChange = (value, index) => {
    const newQuantities = [...selectedIngredientQuantities];
    newQuantities[index] = value;
    setSelectedIngredientQuantities(newQuantities);
  };

  const totalCarbon = searchBars.reduce((acc, _, index) => {
    const carbonPerPortion = selectedIngredients[index] && selectedIngredientQuantities[index] ? 
      (selectedIngredients[index].Total * selectedIngredientQuantities[index]) / numberOfPortions : 0;
    return acc + carbonPerPortion;
  }, 0);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      
      <TextField 
        label="Nom de la recette" 
        value={recipeName} 
        onChange={(e) => setRecipeName(e.target.value)} 
        false
      />
      <TextField 
        label="Nombre de portions" 
        value={numberOfPortions} 
        onChange={(e) => setNumberOfPortions(e.target.value)} 
        true
      />

      <h1 style={{ textAlign: 'left' }}>Empreinte carbone de la recette : {totalCarbon.toFixed(0)} gCO2 par portion </h1>
      

      <h1 style={{ textAlign: 'left' }}>Ingrédients :</h1>
      <table style={{ borderCollapse: 'collapse' }}>
        <thead>
          <tr style={{ border: '1px solid black' }}>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Nom</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Empreinte carbone de l'aliment <br /> (gCO2/g)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Quantité <br /> (g)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Quantité/portion <br /> (g)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Empreinte carbone par portion <br /> (gCO2)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Pourcentage de <br /> l'empreinte carbone</th> 
          </tr>
        </thead>
        <tbody>
          {searchBars.map((_, index) => {
            const carbonPerPortion = selectedIngredients[index] && selectedIngredientQuantities[index] ? 
              ((selectedIngredients[index].Total * selectedIngredientQuantities[index]) / numberOfPortions).toFixed(0) : 0;

            const totalCarbon = searchBars.reduce((acc, _, i) => {
              return acc + (selectedIngredients[i]?.Total * selectedIngredientQuantities[i] || 0) / numberOfPortions;
            }, 0);

            const percentage = totalCarbon ? ((carbonPerPortion / totalCarbon) * 100).toFixed(0) : '---';

            return (
              <tr key={index}>
                <td>
                  <IngredientsArea
                    index={index}
                    ingredients={ingredients}
                    searchQuery={searchQuery}
                    handleSearchQuery={handleSearchQuery}
                    handleSelection={(ingredients) => handleSelection(searchQuery, index)}
                    filteredIngredients={filteredIngredients[index] || []}
                  />
                </td>
                <td>
                  {selectedIngredients[index]?.Total || '---'}
                </td>
                <td>
                  <input 
                    type="number" 
                    value={selectedIngredientQuantities[index] || ''}
                    onChange={(e) => handleQuantityChange(e.target.value, index)} 
                  />
                </td>
                <td>
                  {selectedIngredientQuantities[index] ? (selectedIngredientQuantities[index] / numberOfPortions).toFixed(1) : '---'}
                </td>
                <td>
                  {carbonPerPortion || '---'}
                </td>
                <td>
                  {percentage + ' %'}
                </td>
              </tr>
            );
          })}
        </tbody>
        <tfoot>
          <tr>
            <td>
              <button onClick={addSearchBar}>Ajouter ingrédient</button>
            </td>
            <td></td>
          </tr>
        </tfoot>
      </table>

      <h1 style={{ textAlign: 'left' }}>Etapes :</h1>
      

    </div>
  );
}

export default App;
