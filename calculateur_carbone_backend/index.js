const express = require('express');
const app = express();
const port = 5000;
const fs = require('fs');
const cors = require('cors');

// Lecture de la base de données Agribalyse et conversion en object JS
let rawData = fs.readFileSync('agribalyse_base_donnees.json');
let ingredients = JSON.parse(rawData);

// Utilise le middleware CORS pour autoriser les requêtes cross-origin
app.use(cors());

// Définit une route GET pour "/ingredients" qui renverra la liste des ingrédients en JSON
app.get('/ingredients', (req, res) => {
  res.json(ingredients);
});

// Démarre le serveur sur le port spécifié
app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});